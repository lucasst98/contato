@extends('layout.master')
@section('content')
    <table class="table table-hover">
      <thead>
        <tr>
          <th scope="col">nome</th>
          <th scope="col">contato</th>
          <th scope="col">email</th>
          <th scope="col">ação</th>
        </tr>
      </thead>
      <tbody>
        {{-- @php
          dump($contacts);
        @endphp --}}
        @foreach($contacts as $contact)
          <tr>
            <td scope="row">{{ $contact->nome}}</td>
            <td scope="row">{{ $contact->contato }}</td>
            <td scope="row">{{ $contact->email}}</td>
            <td> 
              <form action="{{ route('contact.delete', $contact->id) }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger" >Excluir</button> 
                <a href={{route('contact.edit', $contact->id)}} type="button" class="btn btn-warning">Editar</button>
              </form> 
            </td>
          </tr>
        @endforeach  
      </tbody>
    </table> 
    <a href="{{route('contact.create')}}" class="btn btn-outline-success">Cadastro</a>
@endsection