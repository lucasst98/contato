<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContatosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/contact', ContatosController::class);
Route::GET('/',[ContatosController::class,'index'])->name('contact.index');
Route::post('delete/{id}',[ContatosController::class,'delete'])->name('contact.delete');
Route::POST('registro',[ContatosController::class,'contactRegister'])->name('contact.register');
