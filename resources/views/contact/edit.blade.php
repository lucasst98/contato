@extends('layout.master')
@section('content')
    <form method="post" action="{{route('contact.update', $contacts->id)}} ">
        @csrf
        @method ('PUT')
            <div class="form-group">
                <label for="name">Nome:</label>
                <input name="nome" type="name" class="form-control" placeholder="nome" value="{{$contacts->nome}}">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input name="email" type="email" class="form-control" placeholder="exemplo@gmail.com" value="{{$contacts->email}}">
            </div>
            <div class="form-group">
                <label for="contact">contato:</label>
                <input name="contato" type="text" class="form-control" placeholder="contato" value="{{$contacts->contato}}">
            <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection