<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'contato' => 'required|max:9'
        ];
    }
    public function messages(){
        return[
            'email.required' => 'O campo email é obrigatório.', 
            'email.email'=>'Email inválido',
        ];
    }
}
