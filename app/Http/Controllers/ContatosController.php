<?php

namespace App\Http\Controllers;

use App\Models\Contatos;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRegisterRequest;

class ContatosController extends Controller
{
    public function contactRegister(ContactRegisterRequest $request)
    {
        $validated = $request->validated();
    
        $user = Contatos::create([
            'nome' => $validated['nome'],
            'email' => $validated['email'],
            'contato' => $validated['contato'],
        ]);
        
        return view ('contact.index');
    }

    public function create()
    {
       return view ('contact.create');
    }

    public function index()
    {
        $contacts = Contatos::where('status',0)->get();
        return view('contact.index',[
            'contacts' => $contacts
        ]);

    }
    
    public function edit($id)
    {
        $contacts = Contatos::where('id',$id)->first();
        return view('contact.edit',[
            'contacts' => $contacts
        ]);
    }

    public function update($id, Request $request)
    {
        $contacts = Contatos::where('id', $id)->first();
        $contacts->update($request->all());
        return redirect()->route('contact.index');
    }

    public function delete($id)
    {
        $contacts = Contatos::where('id', $id)->first(); 
        $contacts->update([
                'status' => 1,
                ]);
       return redirect()->route('contact.index');
    }
}
